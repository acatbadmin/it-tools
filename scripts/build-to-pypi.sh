#!/usr/bin/env bash

scripts_dir="$(dirname "$0")"
repo_dir="$(dirname "$scripts_dir")"
if [[ -z $VIRTUAL_ENV ]]; then
    echo "Error: Need to activate VIRTUAL_ENV."
    exit 1
fi

rm "${repo_dir}/dist/"*
python3 -m build
if [[ $1 == '-n' || $1 == '-s' ]]; then
    echo "simulation mode: no files will be uploaded"
    exit
fi
python3 -m twine upload "${repo_dir}/dist/"*
