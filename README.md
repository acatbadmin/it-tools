# IT Tools

Basic command-line tools used to solve common problems in the Central African Republic.

### exe-check

Quickly determine if EXE or other similar file has been "packed" with extra data;
i.e. it has been corrupted by a virus.



### get-usb-files

Recover "hidden" files on USB drive that are nested away in a series of folders
whose names consiste of a single, non-printable character. On Windows 10 these
folders are invisible to the user, and even when visible (Win11 and non-Windows
systems) they are nearly impossible to find without special knowledge or tools.
This is caused by the
["USB Driver.exe" virus](https://n8marti.gitlab.io/blog/post/2023-04-08-usbdriver-virus/).
